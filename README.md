# debugbox

## `cheaprandom.py`

Used by [`datatestcheap.sh`](#datatestcheapsh).

```
usage: cheaprandom.py [-h] SIZE PATH

Create a file filled with cheap random numbers. The random numbers are *not* sourced from /dev/urandom
or getrandom in order to improve performance. This tool is thus not suited for security critical applications.

positional arguments:
  SIZE        Size of the file, in MiB
  PATH        Path to output file

optional arguments:
  -h, --help  show this help message and exit
```

## `datatestcheap.sh`

```
usage: /usr/local/bin/datatestcheap.sh DIR NFILES MEGS INTERVAL

Check correctness of the storage stack.

positional arguments:
  DIR       Path to output directory (must not exist)
  NFILES    Half of the total number of files to create
  MEGS      Size of each file in MiB
  INTERVAL  sleep(1) time between a single rewrite/check iteration


This tool creates two directories under DIR: stable and rewritten.
In each directory, NFILES files of MEGS MiB size will be created. The
files in the stable subdirectory are only written once at start up,
while the files in the rewritten subdirectory are created once per
iteration.

After writing the files, the tool calculates the SHA256SUMS file for
each subdirectory using sha256sum(1). On each iteration, the SHA256SUMS
are checked against the files as they exist on the disk.

The idea of this tool is to detect silent corruption of data on the
storage, for example during upgrade operations which touch the ceph
cluster.

For representative results, NFILES*MEGS MiB should be chosen larger than
than the amount of RAM available on the worker node.
```

## `rabbitmq-check.py`

```
usage: rabbitmq-check.py [-h] [-u USERNAME] [-p PASSWORD] [-H HOSTNAME] [-P PORT] [-V VHOST] [-d] [-v] check

RabbitMQ bindings consistency check. Uses RabbitMQ Management API endpoint. It pushes test messages to
exchanges that have queue bindings and checks if those messages are routed.

positional arguments:
  check                 Runs binding checks

optional arguments:
  -h, --help            show this help message and exit
  -u USERNAME, --username USERNAME
                        Username to API (defaults to $RABBITMQ_DEFAULT_USER)
  -p PASSWORD, --password PASSWORD
                        Password to API (defaults to $RABBITMQ_DEFAULT_PASS)
  -H HOSTNAME, --hostname HOSTNAME
                        Hostname or IP of the API endpoint
  -P PORT, --port PORT  API Port (HTTPS not supported)
  -V VHOST, --vhost VHOST
                        Virtual Host to operate on (defaults to /)
  -d, --debug           Print debug messages
  -v, --verbose         Print more info
```
