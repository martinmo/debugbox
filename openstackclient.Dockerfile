FROM python:3.12-slim-bookworm@sha256:9638987a16d55a28096d4d222ea267710d830b207ed1a9f0b48ad2dbf67475aa

# NOTE: please keep the list of packages to install sorted for easier visual
# scanning of what is available in the image.
RUN set -eu; \
    export BUILD_PACKAGES="build-essential libssl-dev libffi-dev" ; \
    export REQUIRED_PACKAGES="bash-completion curl jq less procps psmisc screen tini tmux vim" ; \
    apt-get update; \
    apt-get install -y --no-install-recommends ${BUILD_PACKAGES} ${REQUIRED_PACKAGES} ; \
    pip install openstackclient ; \
    pip install osc-placement ; \
    apt-get purge --auto-remove -y ${BUILD_PACKAGES} ; \
    rm -rf /var/lib/apt/lists ; \
    mkdir -p /etc/bash_completion.d/ ; \
    openstack complete | tee /etc/bash_completion.d/osc.bash_completion > /dev/null; \
    echo "alias o=openstack" >> /etc/bash.bashrc

# home is where vim does *not* enable mouse support.
COPY vimrc.local /etc/vim/vimrc.local

CMD ["bash", "-l"]
