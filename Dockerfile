FROM debian:12

RUN mkdir -p /etc/apt/keyrings
COPY apt-key-kubernetes.gpg /etc/apt/keyrings/kubernetes-apt-keyring.gpg
COPY kubernetes.list /root/kubernetes.list

# NOTE: please keep the list of packages to install sorted for easier visual
# scanning of what is available in the image.
RUN set -eu; \
    apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get install -y ca-certificates apt-transport-https; \
    mv /root/kubernetes.list /etc/apt/sources.list.d/; \
    apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
        atop \
        bind9-dnsutils \
        bind9-utils \
        bridge-utils \
        ca-certificates \
        cri-tools \
        curl \
        git \
        golang \
        hdparm \
        htop \
        iftop \
        iotop \
        iperf \
        iproute2 \
        iputils-ping \
        jq \
        less \
        libvirt-clients \
        ltrace \
        mtr-tiny \
        ncdu \
        netcat-openbsd \
        ngrep \
        nmap \
        openssl \
        openvswitch-common \
        pciutils \
        procps \
        psmisc \
        pv \
        python3-openvswitch \
        screen \
        strace \
        sysstat \
        tcpdump \
        tcpflow \
        tmux \
        tree \
        vim \
        ; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists

# home is where vim does *not* enable mouse support.
COPY vimrc.local /etc/vim/vimrc.local
COPY files/rabbitmq-check.py files/datatestcheap.sh /usr/local/bin/
COPY files/cheaprandom.py /
